import time
import undetected_chromedriver as uc
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from bs4 import BeautifulSoup
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import os
import csv
from webdriver_manager.chrome import ChromeDriverManager
import pandas as pd
import ssl

ssl._create_default_https_context = ssl._create_stdlib_context
webdriver_service = Service(ChromeDriverManager().install())
webdriver_service.start()
options = Options()
options.add_argument("--no-sandbox")
options.add_argument("--disable-notifications")
options.add_argument('--headless')
options.add_argument("--disable-gpu")
options.add_argument("--window-size=1440,1080")
options.add_argument('--disable-dev-shm-usage')
driver = uc.Chrome(options=options, service=webdriver_service, version_main=121)


def get_last_page(web_driver, link):
    while True:
        try:
            web_driver.get(link)
            WebDriverWait(web_driver, 10).until(
                EC.presence_of_element_located((By.CSS_SELECTOR, '.pagination'))
            )
            break
        except Exception as e:
            print(e)
    pagination = web_driver.find_element(By.CSS_SELECTOR, '.pagination')
    last_page = pagination.find_elements(By.TAG_NAME, 'li')[-1].find_element(By.TAG_NAME, 'a').get_attribute(
        'data-page')
    return int(last_page)


def get_info_about_company(html_block):
    company_name, website, location, link, rate, reviews = '', '', '', '', '', ''
    is_verified, employees, hourly_rate, company_motto, who_said = '', '', '', '', ''
    min_project_size, agency_type, service_focus = '', '', ''
    company_name = html_block.find('a', class_='company_title directory_profile').text.strip()
    if html_block.find('span', class_='rating sg-rating__number'):
        rate = html_block.find('span', class_='rating sg-rating__number').text.strip()
    if html_block.find('a', class_='reviews-link sg-rating__reviews directory_profile'):
        reviews = html_block.find('a', class_='reviews-link sg-rating__reviews directory_profile').text.strip()
    if html_block.find('a', class_='website-link__item'):
        website = html_block.find('a', class_='website-link__item').get('href').strip()
    if html_block.find('span', class_='locality'):
        location = html_block.find('span', class_='locality').text.strip()
    link = f"https://clutch.co{html_block.find('a', class_='company_title directory_profile').get('href').strip()}"
    for info_div in html_block.find_all('div', class_='list-item custom_popover'):
        if info_div.find('i', class_='list_icon icon_person'):
            employees = info_div.text.strip()
        elif info_div.find('i', class_='list_icon icon_clock'):
            hourly_rate = info_div.text.strip()
    company_motto = html_block.find('blockquote', class_='blockquote-in-module').text.strip()
    if html_block.find('ul', class_='module-list small-list'):
        who_said = html_block.find('ul', class_='module-list small-list').text.strip()
    min_project_size = html_block.find('div', class_='list-item block_tag custom_popover').text.strip()
    agency_type = html_block.find('p', class_='company_info__wrap tagline').text.strip()
    if html_block.find('div', class_='chartAreaContainer spm-bar-chart'):
        service_focus_block = html_block.find('div', class_='chartAreaContainer spm-bar-chart')
        service_focus_parts = [
            f'{i.get("data-content").replace("<i>", "").replace("</i>", " ").replace("<b>", "").replace("</b>", "")}'.strip()
            for i in
            service_focus_block.find_all('div', class_='grid custom_popover')]
        service_focus = ', '.join(service_focus_parts)
    is_verified = 'Yes' if html_block.find('span', class_='verification_icon') else 'No'
    return (company_name, website, location, link, rate, reviews, is_verified, employees, hourly_rate,
            company_motto, who_said, min_project_size, agency_type, service_focus)


def get_info_blocks(html):
    soup = BeautifulSoup(html, 'lxml')
    full_block = soup.find('ul', class_='directory-list')
    company_blocks = full_block.find_all('li', class_='provider provider-row')
    return company_blocks


def get_all_companies_data(url):
    info_list = []
    while True:
        try:
            driver.get(url)
            WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.CSS_SELECTOR, ".list_wrap"))
            )
            break
        except Exception as e:
            print(e)
    page_source = driver.page_source
    info_blocks = get_info_blocks(page_source)
    for block in info_blocks:
        info = get_info_about_company(block)
        info_list.append(info)
    return info_list


if __name__ == '__main__':
    category_link = 'https://clutch.co/agencies/digital'
    last_page = get_last_page(driver, category_link)
    initial_file_name = 'result.csv'
    is_exist_final_file = initial_file_name in os.listdir()
    if not is_exist_final_file:
        with open("result.csv", "w") as file:
            writer = csv.writer(file)
            writer.writerow(
                ("company_name", "website", "location", "link", "rate", "reviews", "is_verified", "employees",
                 "hourly_rate",
                 "company_motto", "who_said", "min_project_size", "agency_type", "service_focus"))
            for i in range(1, last_page + 1):
                print(i)
                for info in get_all_companies_data(f"{category_link}?page={i}"):
                    writer.writerow(info)
    else:
        df = pd.read_csv(initial_file_name)
        need_page = df.__len__() // 50 + 1
        with open("result.csv", "a") as file:
            writer = csv.writer(file)
            for i in range(need_page, last_page + 1):
                print(i)
                for info in get_all_companies_data(f"{category_link}?page={i}"):
                    writer.writerow(info)
