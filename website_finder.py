import time
import undetected_chromedriver as uc
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from bs4 import BeautifulSoup
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager
import os
import csv
import pandas as pd
import ssl

ssl._create_default_https_context = ssl._create_stdlib_context

webdriver_service = Service(ChromeDriverManager().install())
webdriver_service.start()


options = Options()
options.add_argument("--no-sandbox")
options.add_argument("--disable-notifications")
# options.add_argument('--headless')
options.add_argument("--disable-gpu")
options.add_argument("--window-size=1440,1080")
options.add_argument('--disable-dev-shm-usage')
driver = uc.Chrome(options=options,service=webdriver_service,version_main=121)


def get_website_info(webdriver, url):
    while True:
        try:
            webdriver.get(url)
            WebDriverWait(webdriver, 10).until(
                EC.presence_of_element_located((By.CSS_SELECTOR, ".visit-website"))
            )
            break
        except Exception as e:
            time.sleep(5)
            print(e)
    return webdriver.find_element(By.CSS_SELECTOR, ".visit-website").get_attribute('href')


def update_file():
    final_file_name = 'final.csv'
    initial_file_name = 'result.csv'
    is_exist_final_file = final_file_name in os.listdir()
    is_exist_initial_file = initial_file_name in os.listdir()
    if not is_exist_initial_file:
        print("You don't have initial file")
        return
    initial_df = pd.read_csv(initial_file_name, keep_default_na=False)
    if is_exist_final_file:
        df = pd.read_csv(final_file_name)
        sheet_len = df.__len__()
        for index in range(sheet_len, initial_df.__len__()):
            with open('final.csv', 'a') as file:
                writer = csv.writer(file)
                if initial_df.loc[index].get('website') != '':
                    writer.writerow(initial_df.loc[index])
                else:
                    website = get_website_info(driver, initial_df.loc[index].get('link'))
                    row_data = initial_df.loc[index]
                    row_data['website'] = website
                    writer.writerow(row_data)
    else:
        with open(final_file_name, 'w') as file:
            writer = csv.writer(file)
            writer.writerow(
                ("company_name", "website", "location", "link", "rate",
                 "reviews", "is_verified", "employees", "hourly_rate",
                 "company_motto", "who_said", "min_project_size", "agency_type", "service_focus"))
            for index in range(initial_df.__len__()):
                if initial_df.loc[index].get('website') != '':
                    writer.writerow(initial_df.loc[index])
                else:
                    website = get_website_info(driver, initial_df.loc[index].get('link'))
                    row_data = initial_df.loc[index]
                    row_data['website'] = website
                    writer.writerow(row_data)


if __name__ == "__main__":
    update_file()
